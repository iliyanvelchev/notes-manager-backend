const express = require('express')
require('./db/mongoose')
const userRouter = require('./routers/user')
const noteRouter = require('./routers/note')
const cors = require('cors')

const app = express()
app.use(cors())

app.use(express.json())
app.use(userRouter)
app.use(noteRouter)

module.exports = app