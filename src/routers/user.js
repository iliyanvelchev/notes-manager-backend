
const express = require('express')
const User = require('../models/user')
const auth = require('../middleware/auth')
const router = new express.Router()

router.post('/user', async (req, res) => {
    const user = new User(req.body)

    try {
        await user.save()
        const token = await user.generateToken()
        res.status(201).json({ user, token })
    }
    catch (e) {
        console.log(e)

        return res.status(400).json({ message: e.message })
    }
})

router.post('/user/login', async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.generateToken()
        return res.status(200).json({ user, token })
    }
    catch (e) {
        return res.status(400).json({ message: e.message })
    }

})

router.post('/user/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })

        await req.user.save()
        return res.status(200).send()
    }
    catch (e) {
        console.log(e)
        return res.status(500).json({ message: e.message })
    }
})

router.post('/user/logout/all', auth, async (req, res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        return res.status(200).send()
    }
    catch (e) {
        return res.status(500).json({ message: e.message })
    }
})

router.get('/user/me', auth, async (req, res) => {
    return res.status(200).json(req.user)
})

router.patch('/user/me', auth, async (req, res) => {
    const toUpdate = Object.keys(req.body)
    const allowed = ['name', 'email', 'password', 'age']
    const invalid = toUpdate.find((update) => !allowed.includes(update))
    if (invalid) {
        return res.status(400).json({ message: 'Property ' + invalid + ' cannot be modified' })
    }
    try {
        toUpdate.forEach((update) => req.user[update] = req.body[update])
        await req.user.save()
        return res.status(200).json(req.user)
    }
    catch (e) {
        return res.status(500).json({ message: e.message })
    }
})

router.delete('/user/me', auth, async (req, res) => {
    try {
        await req.user.remove()
        return res.status(200).send()
    }
    catch (e) {
        return res.status(500).json({ message: e.message })
    }
})

module.exports = router