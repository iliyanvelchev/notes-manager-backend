
const express = require('express')
const Note = require('../models/note')
const auth = require('../middleware/auth')
const router = new express.Router()

router.post('/note', auth, async (req, res) => {
    const note = new Note({
        payload: req.body.payload,
        owner: req.user._id
    })
    try {
        await note.save()
        return res.status(201).json(note)
    }
    catch (e) {
        return res.status(400).json({ message: e.message })
    }
})

router.get('/note/id/:id', auth, async (req, res) => {
    try {
        const note = await Note.findOne({ _id: req.params.id, owner: req.user._id })

        if (!note) {
            res.status(404).json()
        }
        return res.status(200).json(note)
    }
    catch (e) {
        return res.status(500).json({ message: e.message })
    }
})

router.get('/note/all', auth, async (req, res) => {
    try {
        await req.user.populate({
            path: 'notes'
        }).execPopulate()
        return res.status(200).json(req.user.notes)
    }
    catch (e) {
        return res.status(500).json({ message: e.message })
    }
})

router.patch('/note/id/:id', auth, async (req, res) => {
    try {
        const note = await Note.findOne({ _id: req.params.id, owner: req.user._id })
        note['payload'] = req.body.payload
        await note.save()
        return res.status(200).json(note)
    }
    catch (e) {
        return res.status(400).json({ message: e.message })
    }
})

router.delete('/note/id/:id', auth, async (req, res) => {
    try {
        const note = await Note.findOneAndDelete({ _id: req.params.id, owner: req.user._id })
        if (!note) {
            return res.status(404).json()
        }
        return res.status(200).json()
    }
    catch (e) {
        return res.status(500).json(e.message)
    }
})

module.exports = router