const mongoose = require('mongoose')

const noteSchema = new mongoose.Schema({
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    payload: {
        type: String,
        required: true
    }
}, { timestamps: true })

noteSchema.methods.toJSON = function () {
    const note = this
    const noteObject = note.toObject()

    delete noteObject.__v

    return noteObject
}

const Note = mongoose.model('Note', noteSchema)

module.exports = Note